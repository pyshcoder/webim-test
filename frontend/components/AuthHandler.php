<?php
namespace frontend\components;

use common\models\User;
use common\models\Auth;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');
        $username = ArrayHelper::getValue($attributes, 'screen_name');
        if($username == '') {
            $username = '_user' . $id;
        }
        $firstName = ArrayHelper::getValue($attributes, 'first_name');
        $lastName = ArrayHelper::getValue($attributes, 'last_name');
        
        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var User $user */
                $user = $auth->user;
                $user->firstName = $firstName;
                $user->lastName = $lastName;
                $user->save();
                
                Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
            } else { // signup
                $password = Yii::$app->security->generateRandomString(6);
                $user = new User([
                    'username' => $username,
                    'vkId' => $id,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'password' => $password,
                ]);
                $user->generateAuthKey();

                $transaction = User::getDb()->beginTransaction();

                if ($user->save()) {
                    $auth = new Auth([
                        'user_id' => $user->id,
                        'source' => $this->client->getId(),
                        'source_id' => (string)$id,
                    ]);
                    if ($auth->save()) {
                        $transaction->commit();
                        Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                    } else {
                        Yii::$app->getSession()->setFlash('error', ['Some problems with saving your account, try again later.']);
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', ['Some problems with saving your account, try again later.']);
                }
            }
        } else { // user already logged in
            Yii::$app->getSession()->setFlash('error', ['You are already logged in!']);
        }
    }
}