<?php

use yii\helpers\Html;
use yii\authclient\widgets\AuthChoice;

/* @var $this yii\web\View */
/* @var $friends array */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php if(\Yii::$app->user->isGuest): ?>
        <div class="jumbotron">
            <h1>Авторизация</h1>

            <p class="lead">Войдите используя ВКонтакте чтобы продолжить</p>
            
            <hr class="my-4">
            
            <?php $authAuthChoice = AuthChoice::begin([
                'baseAuthUrl' => ['site/auth'],
                'popupMode' => true,
            ]); ?>
                <p><?= $authAuthChoice->clientLink($authAuthChoice->getClients()['vkontakte'], 'Войти', ['class' => 'btn btn-lg btn-primary']) ?></p>
            <?php AuthChoice::end(); ?>
        </div>
    <?php else: ?>
        <?php if(count($friends) > 0): ?>
            <h3>Добрый день, <?= \Yii::$app->user->identity->getFullName() ?></h3>
            <?php foreach($friends as $friend): ?>
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="<?= isset($friend['photo_200']) ? $friend['photo_200'] : '/images/no-avatar.png' ?>" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= $friend['first_name'] ?>  <?= $friend['last_name'] ?></h5>
                                <p class="card-text">
                                    <?php if($friend['online'] == 1): ?>
                                        <small class="text-success">Online</small>
                                    <?php elseif(isset($friend['last_seen'])): ?>
                                        <small class="text-muted">Последний раз в сети <?= \Yii::$app->formatter->asRelativeTime($friend['last_seen']['time']) ?></small>
                                    <?php endif; ?>
                                </p>
                                <a href="https://vk.com/id<?= $friend['id'] ?>" class="btn btn-primary" target="_blank">Страница</a>
                                <a href="https://vk.com/write<?= $friend['id'] ?>" class="btn btn-info" target="_blank">Написать</a>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="alert alert-warning" role="alert">Не удалось загрузить друзей :(</div>
        <?php endif; ?>
    <?php endif; ?>
</div>
