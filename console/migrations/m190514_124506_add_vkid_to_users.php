<?php

use yii\db\Migration;

/**
 * Class m190514_124506_add_vkid_to_users
 */
class m190514_124506_add_vkid_to_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'vkId', $this->string(16)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'vkId');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190514_124506_add_vkid_to_users cannot be reverted.\n";

        return false;
    }
    */
}
