<?php

use yii\db\Migration;

/**
 * Class m190517_090123_add_names_to_user
 */
class m190517_090123_add_names_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'firstName', $this->string(128)->defaultValue(null));
        $this->addColumn('{{%user}}', 'lastName', $this->string(128)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'firstName');
        $this->dropColumn('{{%user}}', 'lastName');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190517_090123_add_names_to_user cannot be reverted.\n";

        return false;
    }
    */
}
